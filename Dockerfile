FROM node:18-alpine AS build
# Use production node environment by default.
WORKDIR /app
COPY . .

RUN yarn
RUN yarn build

FROM nginx:1.18-alpine AS deploy
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /app/public .
ENTRYPOINT ["nginx", "-g","daemon off;"]
