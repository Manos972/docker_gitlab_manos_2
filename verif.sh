if ! docker exec container_apache ping -w 10 gatsby_container; then
echo "Le deuxième conteneur n'a pas pu 'pinguer' le premier conteneur."
#exit 1
fi
if ! docker exec container_apache sh -c "wget -O - http://gatsby_container:80 | grep -q 'Gatsby'"; then
echo "Le mot 'Gatsby' n'a pas été trouvé sur la page du premier conteneur."
#exit 1
fi
